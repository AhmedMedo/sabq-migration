<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Media extends Model
{
    //

    protected $connection = 'mongodb';
	protected $collection = 'Media';
    protected $primaryKey = "_id";

    protected $casts = [
        'material' => 'string',
    ];

    public function getCaption()
    {
        return $this->caption;

    }

    public function getCoverPhoto()
    {
        return $this->coverPhoto;
    }

}
