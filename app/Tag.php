<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Tag extends Model
{
    //
    protected $connection = 'mongodb';
	protected $collection = 'Tag';

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
}
