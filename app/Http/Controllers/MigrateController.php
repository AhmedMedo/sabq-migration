<?php

namespace App\Http\Controllers;

use App\User;
use App\Media;
use App\Video;
use App\Material;
use Illuminate\Http\Request;
use DateTime;

class MigrateController extends Controller
{
    //

    public function materials()
    {

    $materials = Material::where('createdAt','>=',new DateTime('2022-01-25T08:40:12.569Z'))
    ->whereIn('status',['published', 'unpublished','new'])
    ->orderBy('createdAt','DESC')
    ->count();

    $site_domain = 'sabq.org';
    $protocol = 'https';
    $full=[];

    foreach($materials as $material)
    {
        if(is_null($material->getCategory()))
        {
            continue;
        }
        // try {
        //     $material->getCategory()->getId();
        // } catch (\Throwable $th) {
        //     //throw $th;
        //     echo $material->getId();
        // }
        // dd($material->getContent());
        $default_image = $protocol . '://' . $site_domain . '/' . $material->getDefaultPhoto();
        if($material->getContent() == '')
        {
            $body = '<p><img src="'.$default_image.'" alt="Some image caption"></p>';
            $story_template = 'photo';

        }else{
            $body = $material->getContent();
            $story_template = 'text';
        }


        $jsonData= [];
        $jsonData['external-id'] = $material->getId();
        $jsonData['headline'] = $material->getMainTitle();
        if(!is_null($material->getSecondaryTitle()))
        {
            $jsonData['subheadline'] = $material->getSecondaryTitle();
        }
        $jsonData['slug'] = mb_strtolower($material->getSlug(),'UTF8');
        // $jsonData['body'] = $body;
        $jsonData['summary'] = $material->getContentString();
        $jsonData['first-published-at'] = $material->getPublishedAt();
        $jsonData['last-published-at'] =$material->getPublishedAt();
        $jsonData['published-at'] =$material->getPublishedAt();
        // $jsonData['temporary-hero-image-url'] = $this->container->getParameter('httpProtocol') . '://' . $this->container->getParameter('site_domain') . '/' . $material->getDefaultPhoto();
        $jsonData['temporary-hero-image-url'] = $default_image;

        $jsonData['status'] = 'published';
        $authors = $material->getAuthor();
        foreach ($authors as $author) {
            $authorArray=[];
            $authorArray['email'] = $author->getEmail();
            $authorArray['name'] = $author->getUsername();
            $authorArray['username'] = $author->getUsername();
            $authorArray['external-id'] = $author->getId();
            $jsonData['authors'][]=$authorArray;
        }
        $sections=[];
        $sections['external-id'] = $material->getCategory()->getId();
        $sections['name'] = $material->getCategory()->getName();
        $sections['slug'] = $material->getCategory()->getSlug();
        if ($material->getCategory()->getParent()) {
            $sections['parent']['external-id'] = $material->getCategory()->getParent()->getId();
            $sections['parent']['name'] = $material->getCategory()->getParent()->getName();
            $sections['parent']['slug'] = $material->getCategory()->getParent()->getSlug();
        }

        $jsonData['sections'][] = $sections;

        $tags = $material->getTag();
        if ($tags) {
            foreach ($tags as $tag) {
                if($tag->getName() != ""  || $tag->getSlug() != "")
                {
                    $tagArray= [];
                    $tagArray['external-id'] = $tag->getId();
                    $tagArray['name'] = $tag->getName();
                    $tagArray['slug'] = $tag->getSlug();
                    $jsonData['tags'][]=$tagArray;
                }

            }
        }

        $jsonData['seo']['meta-description'] = $this->getShortDescription($material->getContent(), 150);
        $jsonData['seo']['meta-title'] = $this->getShortDescription($material->getMainTitle(), 150);
        $jsonData['story-template'] = $story_template;


        $jsonData['story-elements'][]=[
            'type'=>'text',
            'subtype' => "",
            'metadata'=>(object)[],
            'text'=>$body
        ];
        $images = $material->images();
        if(count($images)> 0){
            $cards = array();

            foreach ($images as $image) {
                    $imageCaption = $image->getCaption();
                    if (!$imageCaption) {
                        $imageCaption = '';
                    }
                    if ($image->getCoverPhoto()) {
                        continue;
                    }
                    $elements = [];
                    $elements = array(
                        'type' => 'image',
                        'url' =>$protocol . '://' . $site_domain . '/'.'uploads/material-file/' . $material->getId() . '/'.$image->path,
                        'subtype' => "",
                        'title' => $imageCaption,
                        'description' => "",
                        'link-url' => "",
                    );
                    $jsonData['story-elements'][] = $elements;
            }

                // $cards["type"] = "composite";
                // $cards["subtype"] = "image-gallery";
                // $cards["metadata"]['type'] = "slideshow";
                //            $jsonData['cards'][]= $cards;
        }

        $videos = $material->videos();
        if(count($videos)> 0){

            foreach ($videos as $video) {
                    $elements = [];
                    $elements = array(
                        'type' => 'youtube-video',
                        'url' =>$video->getPath(),
                        'subtype' => null,
                    );
                    $jsonData['story-elements'][] = $elements;
            }

                // $cards["type"] = "composite";
                // $cards["subtype"] = "image-gallery";
                // $cards["metadata"]['type'] = "slideshow";
                //            $jsonData['cards'][]= $cards;
        }
        // // array_push($full , $jsonData);

        // $this->appendToFile(json_encode($jsonData , JSON_UNESCAPED_UNICODE),$file_name);
        array_push($full , $jsonData);
        unset($jsonData);

    }

    // dd($full);

        return response()->json($full);

    }


    private function appendToFile($data,$file_name)
    {
        if (!file_exists(__DIR__."/Output")) {
            mkdir(__DIR__."/Output", 0777, true);
        }
        file_put_contents(__DIR__."/Output".'/'.$file_name, $data."\n", FILE_APPEND | LOCK_EX);
        $file = __DIR__."/Output".'/'.$file_name;
        $gzfile = __DIR__."/Output".'/'.$file_name.'.gz';
        $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
        gzwrite($fp, file_get_contents($file));
        gzclose($fp);

    }

    private function deleteFile($file_name){
        $file = __DIR__."/Output".'/'.$file_name;
        unlink($file);
    }

    public function getShortDescription($content, $length = 260) {
        $shortDescription = trim(html_entity_decode(strip_tags(nl2br($content))));
        if (strlen($shortDescription) > $length) {
            $shortDescription = mb_substr($shortDescription, 0, $length, 'utf-8');
        }
        return $shortDescription;
    }
}
