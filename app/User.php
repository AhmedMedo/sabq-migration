<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model;

class User extends Model
{
    use Notifiable;

    protected $connection = 'mongodb';
	protected $collection = 'User';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getFullname() {
        if (isset($this->username)) {
            return $this->fullname;
        }
        return $this->getShortName($this->fullname, 41);
    }

    public function getUsername() {
        return $this->username;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getEmail()
    {
        return $this->email;
    }

    public function getRealWebPath() {
        $image = $this->image;
        return NULL === $image ? NULL : $this->getUploadDir() . '/' . $image;
    }

    public function getUploadDir() {
        return 'uploads/users-profile-images';
    }

    public function getFullImage()
    {
        if(is_null($this->image))
        {
            return false;
        }

        return 'https://sabq.org/'.$this->getUploadDir().'/'.$this->image;
    }


    public function getShortName($content, $length = 40)
    {
        $shortName = trim(html_entity_decode(strip_tags($content)));
        if (strlen(utf8_decode($shortName)) > $length) {
            $shortName = mb_substr($shortName, 0, $length, 'utf-8') . '..';
        }
        return $shortName;
    }
}
