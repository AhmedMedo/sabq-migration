<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class Category extends Model
{
    use HybridRelations;

    //
    protected $connection = 'mongodb';
	protected $collection = 'CategoryLast';


    public function getId()
    {
        return $this->id;
    }


    public function r_parent()
    {
        return $this->belongsTo($this , 'parent','_id');
    }

    public function getParent()
    {
        return $this->r_parent;
    }

        /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

        /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
