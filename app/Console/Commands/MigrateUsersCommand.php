<?php

namespace App\Console\Commands;

use App\User;
use App\Counter;
use Illuminate\Console\Command;
use DateTime;

class MigrateUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach(range(1,3) as $number){

            $skip = ($number - 1) * 1000;
            // $file_number = sprintf("%02d",$num->counter);
            $file_number = str_pad($number, 5, '0', STR_PAD_LEFT);
            $file_name = 'users-sabq-'.$file_number.'.txt';

            $this->info($file_name);

            $users = User::where('createdAt','>=',new DateTime('2022-01-25T08:40:12.569Z'))
            ->take(1000)
            ->skip($skip)
            ->get();
            foreach($users as $user)
            {

                $jsonData['name'] = $user->getFullname() == "" ? $user->email : $user->getFullname();
                $jsonData['external-id'] = $user->getId();
                $jsonData['email'] = $user->email;
                $jsonData['username'] = $user->email;
                if($user->getFullImage())
                {
                    $jsonData['avatar-url'] = $user->getFullImage();
                }

                if($user->brief)
                {
                    $jsonData['bio'] = $user->brief;

                }
                $this->appendToFile(json_encode($jsonData , JSON_UNESCAPED_UNICODE),$file_name);

                unset($jsonData);

            }
            $this->deleteFile($file_name);
        }

    }


    private function appendToFile($data,$file_name)
    {
        if (!file_exists(__DIR__."/Output")) {
            mkdir(__DIR__."/Output", 0777, true);
        }
        file_put_contents(__DIR__."/Output".'/'.$file_name, $data."\n", FILE_APPEND | LOCK_EX);
        $file = __DIR__."/Output".'/'.$file_name;
        $gzfile = __DIR__."/Output".'/'.$file_name.'.gz';
        $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
        gzwrite($fp, file_get_contents($file));
        gzclose($fp);

    }

    private function deleteFile($file_name){
        $file = __DIR__."/Output".'/'.$file_name;
        unlink($file);
    }
}
