<?php

namespace App\Console\Commands;

use App\Material;
use Illuminate\Console\Command;
use DateTime;
class UpdateStoriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:stories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $materials = Material::where('createdAt','>=',new DateTime('2022-01-25T08:40:12.569Z'))
        ->whereIn('category',[
            new \MongoDB\BSON\ObjectID('55ec4f01b01df06a168b4629') ,
            ])
        ->get();

        $this->info(count($materials).' Will be updated ');

        foreach($materials as $material)
        {
            $material->category = new \MongoDB\BSON\ObjectID('55ec4f00b01df06a168b4627');
            $material->save();
        }
    }
}
