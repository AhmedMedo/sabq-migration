<?php

namespace App\Console\Commands;

use App\Media;
use App\Video;
use App\Material;
use Illuminate\Console\Command;
use DateTime;

class MediaMigrateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'media:migrate {index}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', -1);

        foreach(range(1,5) as $number){

            $skip = ($number - 1) * 1000;

            $materials = Material::where('createdAt','>=',new DateTime('2022-01-25T08:40:12.569Z'))
            ->whereIn('status',['published', 'unpublished','new'])
            ->orderBy('createdAt','DESC')
            ->take(1000)
            ->skip($skip)
            ->get();

            $file_number = str_pad($number, 5, '0', STR_PAD_LEFT);
            $file_name = 'story-sabq-'.$file_number.'.txt';

            $this->info('file Number is now generating '.$file_name);
            $site_domain = 'sabq.org';
            $protocol = 'https';
            $full=[];
            foreach($materials as $material)
            {
                if(is_null($material->getCategory()))
                {
                    continue;
                }
                $default_image = $protocol . '://' . $site_domain . '/' . $material->getDefaultPhoto();
                if($material->getContent() == '')
                {
                    $body = '<p><img src="'.$default_image.'" alt="Some image caption"></p>';
                    $story_template = 'photo';

                }else{
                    $body = $material->getContent();
                    $story_template = 'text';
                }



                $jsonData= [];
                $jsonData['external-id'] = $material->getId();
                $jsonData['headline'] = $material->getMainTitle();
                if(!is_null($material->getSecondaryTitle()))
                {
                    $jsonData['subheadline'] = $material->getSecondaryTitle();
                }
                $jsonData['slug'] = mb_strtolower($material->getSlug(),'UTF8');
                // $jsonData['body'] = $body;
                $jsonData['summary'] = $material->getContentString();
                $jsonData['first-published-at'] = $material->getPublishedAt();
                $jsonData['last-published-at'] =$material->getPublishedAt();
                $jsonData['published-at'] =$material->getPublishedAt();
                // $jsonData['temporary-hero-image-url'] = $this->container->getParameter('httpProtocol') . '://' . $this->container->getParameter('site_domain') . '/' . $material->getDefaultPhoto();
                $jsonData['temporary-hero-image-url'] = $default_image;

                $jsonData['status'] = 'published';
                $authors = $material->getAuthor();
                foreach ($authors as $author) {
                    $authorArray=[];
                    $authorArray['email'] = $author->getEmail();
                    $authorArray['name'] = $author->getUsername();
                    $authorArray['username'] = $author->getUsername();
                    $authorArray['external-id'] = $author->getId();
                    $jsonData['authors'][]=$authorArray;
                }
                $sections=[];
                $sections['external-id'] = $material->getCategory()->getId();
                $sections['name'] = $material->getCategory()->getName();
                $sections['slug'] = $material->getCategory()->getSlug();
                if ($material->getCategory()->getParent()) {
                    $sections['parent']['external-id'] = $material->getCategory()->getParent()->getId();
                    $sections['parent']['name'] = $material->getCategory()->getParent()->getName();
                    $sections['parent']['slug'] = $material->getCategory()->getParent()->getSlug();
                }

                $jsonData['sections'][] = $sections;

                $tags = $material->getTag();
                if ($tags) {
                    foreach ($tags as $tag) {
                        if($tag->getName() != ""  || $tag->getSlug() != "")
                        {
                            $tagArray= [];
                            $tagArray['external-id'] = $tag->getId();
                            $tagArray['name'] = $tag->getName();
                            $tagArray['slug'] = $tag->getSlug();
                            $jsonData['tags'][]=$tagArray;
                        }

                    }
                }

                $jsonData['seo']['meta-description'] = $this->getShortDescription($material->getContent(), 150);
                $jsonData['seo']['meta-title'] = $this->getShortDescription($material->getMainTitle(), 150);
                $jsonData['story-template'] = $story_template;


                $jsonData['story-elements'][]=[
                    'type'=>'text',
                    'subtype' => "",
                    'metadata'=>(object)[],
                    'text'=>$body
                ];
                $images = $material->images();
                if(count($images)> 0){

                    foreach ($images as $image) {
                            $imageCaption = $image->getCaption();
                            if (!$imageCaption) {
                                $imageCaption = '';
                            }
                            if ($image->getCoverPhoto()) {
                                continue;
                            }
                            $elements = [];
                            $elements = array(
                                'type' => 'image',
                                'url' =>$protocol . '://' . $site_domain . '/'.'uploads/material-file/' . $material->getId() . '/'.$image->path,
                                'subtype' => "",
                                'title' => $imageCaption,
                                'description' => "",
                                'link-url' => "",
                            );
                            $jsonData['story-elements'][] = $elements;
                    }
                }

                $videos = $material->videos();
                if(count($videos)> 0){

                    foreach ($videos as $video) {
                            $elements = [];
                            $elements = array(
                                'type' => 'youtube-video',
                                'url' =>$video->getPath(),
                                'subtype' => null,
                            );
                            $jsonData['story-elements'][] = $elements;
                    }

                }

                $this->appendToFile(json_encode($jsonData , JSON_UNESCAPED_UNICODE),$file_name);
                array_push($full , $jsonData);
                unset($jsonData);

            }

            $this->deleteFile($file_name);
        }

    }


    private function appendToFile($data,$file_name)
    {
        if (!file_exists(__DIR__."/Output/final/".$this->argument('index'))) {
            mkdir(__DIR__."/Output/final/".$this->argument('index'), 0777, true);
        }
        file_put_contents(__DIR__."/Output/final/".$this->argument('index').'/'.$file_name, $data."\n", FILE_APPEND | LOCK_EX);
        $file = __DIR__."/Output/final/".$this->argument('index').'/'.$file_name;
        $gzfile = __DIR__."/Output/final/".$this->argument('index').'/'.$file_name.'.gz';
        $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
        gzwrite($fp, file_get_contents($file));
        gzclose($fp);

    }

    private function deleteFile($file_name){
        $file = __DIR__."/Output/final/".$this->argument('index').'/'.$file_name;
        unlink($file);
    }

    public function getShortDescription($content, $length = 260) {
        $shortDescription = trim(html_entity_decode(strip_tags(nl2br($content))));
        if (strlen($shortDescription) > $length) {
            $shortDescription = mb_substr($shortDescription, 0, $length, 'utf-8');
        }
        return $shortDescription;
    }
}
