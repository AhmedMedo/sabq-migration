<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Video extends Model
{
    //
    protected $connection = 'mongodb';
	protected $collection = 'Video';
    protected $primaryKey = "_id";

    protected $casts = [
        'material' => 'string',
    ];

    public function getPath()
    {
        return 'https://www.youtube.com/watch?v='.$this->vid;
    }
}
