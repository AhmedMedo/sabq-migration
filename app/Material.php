<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Material extends Model
{

    //
    protected $connection = 'mongodb';
	protected $collection = 'Material';
    protected $dates = ['createdAt','publishedAt'];
    protected $primaryKey = "_id";
    protected $fillable = ['_id','categroy'];


    // protected $fillable = [
    //     'category'
    // ];
    // public function author()
    // {
    //     return $this->belongsTo(Author::class,'null','author','_id');
    // }

    public function r_category()
    {
        return $this->belongsTo(Category::class,'category','_id');
    }




    public function getCategory()
    {
        return $this->r_category;
    }

    // public function images()
    // {
    //     return $this->hasMany(Media::class , 'material','_id');
    // }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId() {
        return $this->id;
    }
    public function getContent() {
        return $this->content;
    }

    /**
     * Get mainTitle
     *
     * @return string $mainTitle
     */
    public function getMainTitle() {
        return $this->mainTitle;
    }


    /**
     * Get secondaryTitle
     *
     * @return string $secondaryTitle
     */
    public function getSecondaryTitle() {
        return $this->secondaryTitle;
    }

    /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug() {
        return $this->slug;
    }

        /**
     * Get contentString
     *
     * @return string $contentString
     */
    public function getContentString() {
        return $this->contentString;
    }

        /**
     * @return string the relative path of file starting from web directory
     */
    public function getDefaultPhoto() {
        if (!$this->defaultCoverPhoto) {
            return null;
        }
        if (strpos($this->defaultCoverPhoto, 'default') !== false) {
            return 'default-cover-photo/' . $this->defaultCoverPhoto;
        }

        return 'uploads/material-file/' . $this->getId() . '/' . $this->defaultCoverPhoto;
    }

    /**
     * Get publishedAt
     *
     * @return date $publishedAt
     */
    public function getPublishedAt() {
        return strtotime($this->publishedAt)*1000;
    }

    public function getAuthor()
    {
        // $ids = $this->author;
        if($this->author)
        {
            $authors = User::whereIn('_id',$this->author)->get();
            return $authors;

        }
        return [];
    }


    public function getTag()
    {
        if($this->tag)
        {
            $tags = Tag::whereIn('_id',$this->tag)->get();
            return $tags;

        }

        return [];
    }


    public function images()
    {
        $images = Media::where('material',new \MongoDB\BSON\ObjectID($this->_id))->get();
        return $images;
    }

    public function videos()
    {
        $videos = Video::where('material',new \MongoDB\BSON\ObjectID($this->_id))->get();
        return $videos;
    }
}
