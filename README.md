# Laravel app used to migrate Sabq MongoDB into json format used for data migration for CMS developed by qunitype
## steps
- clone the repo
- run composer install
- make sure you have install mongodb module for php

## commands
the next command will migrate stories will its media and add index to represet the folder number
```
php artisan media:migrate {index}
```
the next command used to migrate users
```
php artisan migrate:users
```
the next command i used to update stories categories for specific conditions
```
php artisan update:stories
```
You can see video to proof the concept form [here](https://drive.google.com/file/d/1AKtTrTq-O-4B3QzCl_XxiY7MJAGFciNq/view?usp=sharing).


